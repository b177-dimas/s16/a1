// console.log("Hello World");

let number = Number(prompt("Input a number"));

console.log("The number you provided is " + number);

for(let i = number; i >= 50; i--){
	if (i === 50){
		console.log("The current value is at 50. Terminating loop.");
		continue;
	};

	if(i % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};

	if (i % 5 === 0){
		console.log(i);
		continue;
	}
};


let myString = "supercalifragilisticexpialidocious";
console.log(myString);

let consonants = "";

for(let i = 0; i < myString.length; i++){

	if (myString[i].toLowerCase() == "a" ||
		myString[i].toLowerCase() == "e" ||
		myString[i].toLowerCase() == "i" ||
		myString[i].toLowerCase() == "o" ||
		myString[i].toLowerCase() == "u" 
	){
		continue;
	}
	else{
		consonants += myString[i] + "";
	}
};

	console.log(consonants);